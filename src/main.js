import { createApp } from 'vue';
import router from './router';
import App from './App.vue';

import { library } from "@fortawesome/fontawesome-svg-core";
import { faHome, faFileAlt, faDollarSign } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

import 'bootstrap/scss/bootstrap.scss'
import 'bootstrap';
import '@/assets/scss/style.scss'

library.add(faFileAlt, faDollarSign, faHome);

createApp(App).component("font-awesome-icon", FontAwesomeIcon).use(router).mount('#app')
